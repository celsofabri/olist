const password = document.getElementById('password');
const passwordConfirm = document.getElementById('password_confirm');

const passwordMin = document.getElementById('password-min');
const passwordUppercase = document.getElementById('password-uppercase');
const passwordNumber = document.getElementById('password-number');
const passwordEqual = document.getElementById('password-equal');

const levels = [].slice.call(document.querySelectorAll('.password__level'));
const backgroundLevelDefault = levels[0].style.backgroundColor;

const colors = {
  default: '#EAEAF4',
  low: '#f79682',
  medium: '#F7BC1C',
  high: '#1FE6A8'
}

const rules = {
  number: false,
  capital: false,
  length: false
}

password.oninput = () => {

  if(password.value.length > 5) {
    passwordMin.classList.add('password__item--verify');
    rules.length = true;
  } else {
    passwordMin.classList.remove('password__item--verify');
    rules.length = false;
  }

  if (password.value.match(/[A-Z]/g)) {
    passwordUppercase.classList.add('password__item--verify');
    rules.capital = true;
  } else {
    passwordUppercase.classList.remove('password__item--verify');
    rules.capital = false;
  }

  if (password.value.match(/[0-9]/g)) {
    passwordNumber.classList.add('password__item--verify');
    rules.number = true;
  } else {
    passwordNumber.classList.remove('password__item--verify');
    rules.number = false;
  }

  updatePasswordLevel();
}

function updatePasswordLevel() {
  let score = 0;

  Object.keys(rules).map((key) => {
    const rule = rules[key];

    if(rule) {
      score++
    }
  });

  levels.map(level => level.style.backgroundColor = backgroundLevelDefault);
  password.classList.remove('form__input--low');
  password.classList.remove('form__input--medium');
  password.classList.remove('form__input--high');

  if(score === 1) {
    levels[0].style.backgroundColor = colors.low;
    password.classList.add('form__input--low');
  } else if(score === 2) {
    levels[0].style.backgroundColor = colors.medium;
    levels[1].style.backgroundColor = colors.medium;
    password.classList.add('form__input--medium');
  } else if(score === 3) {
    levels[0].style.backgroundColor = colors.high;
    levels[1].style.backgroundColor = colors.high;
    levels[2].style.backgroundColor = colors.high;
    password.classList.add('form__input--high');
  }
}

passwordConfirm.oninput = () => {
  if(password.value === passwordConfirm.value) {
    passwordEqual.style.display = 'none';
  } else {
    passwordEqual.style.display = 'block';
  }
}
